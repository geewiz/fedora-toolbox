# Custom image for fedora-toolbox

This is a custom Fedora Workstation image for use with
[Toolbox](https://github.com/containers/toolbox).

## Host setup

For the `podman` and `docker` commands to work inside the toolbox container,
enable the Podman socket on the host:

```shell
sudo systemctl --user enable podman.socket
sudo systemctl --user start podman.socket
```
