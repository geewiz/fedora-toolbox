ARG FEDORA_VERSION=40
FROM registry.fedoraproject.org/fedora-toolbox:${FEDORA_VERSION}

ARG SMUG_RELEASE="0.3.3"
ENV SMUG_RELEASE_URL="https://github.com/ivaaaan/smug/releases/download/v${SMUG_RELEASE}/smug_${SMUG_RELEASE}_Linux_x86_64.tar.gz"

COPY packages.txt /
RUN dnf -y upgrade \
		&& dnf -y install $(< /packages.txt) \
			"@C Development Tools and Libraries" \
			"@Development Tools" \
		&& dnf clean all && rm /packages.txt

RUN ln -s /usr/bin/podman-remote /usr/local/bin/podman \
    && ln -s /usr/bin/podman-remote /usr/local/bin/docker

RUN BUILDDIR=$(mktemp -d); \
    curl -Ls "${SMUG_RELEASE_URL}" | tar -xz -C $BUILDDIR -f - \
    && mv $BUILDDIR/smug /usr/local/bin/ && rm -rf $BUILDDIR
